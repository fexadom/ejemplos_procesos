#include <unistd.h>
#include <stdio.h>

int main()
{
	if(fork() == 0)
		printf("Hijo\n");
	else
		printf("Padre\n");

	printf("Padre e hijo\n");
}
